package com.example.ponomarenkopavel.gefira_redminecontacts.common.adapters;

import android.support.annotation.NonNull;
import android.support.v4.util.ArrayMap;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.BaseViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.holder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public class AppBaseAdapter extends RecyclerView.Adapter<BaseViewHolder<BaseViewModel>> {

    private List<BaseViewModel> list = new ArrayList<>();

    private ArrayMap<Integer, BaseViewModel> typeInstances = new ArrayMap<>();

    @NonNull
    @Override
    public BaseViewHolder<BaseViewModel> onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return typeInstances.get(i).createViewHolder(viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder<BaseViewModel> holder, int i) {
        holder.bindViewHolder(getItem(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getType().getValue();
    }

    public void registerTypeInstance(BaseViewModel item) {
        if (!typeInstances.containsKey(item.getType()));
        typeInstances.put(item.getType().getValue(), item);
    }

    public void addItems(List<? extends BaseViewModel> newItems) {

        for (BaseViewModel newItem : newItems) {
            registerTypeInstance(newItem);
        }

        list.addAll(newItems);
        notifyDataSetChanged();
    }

    public void setItems(List<BaseViewModel> items) {
        clearList();
        addItems(items);
    }

    public void editItem(int position) {

    }

    public void refreshBlockOverlay(int position) {
        notifyItemChanged(position);
    }

    public void clearList() {
        list.clear();
    }

    public BaseViewModel getItem(int position) {
        return list.get(position);
    }

    public int getRealItemCount() {
        int count = 0;
        for (int i = 0; i < getItemCount(); i++) {
            if (!getItem(i).isItemDecorator()) {
                count += 1;
            }
        }
        return count;
    }
}
