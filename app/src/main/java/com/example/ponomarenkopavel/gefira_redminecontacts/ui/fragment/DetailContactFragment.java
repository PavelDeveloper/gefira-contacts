package com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.BaseContactPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.DetailContactPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.DetailView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailContactFragment extends BaseFragment implements DetailView {

    @InjectPresenter
    DetailContactPresenter presenter;

    @BindView(R.id.imageView5)
    ImageView profileIv;

    @BindView(R.id.name_lastName_textView)
    TextView fullNameTv;

    @BindView(R.id.phoneImageView)
    ImageView phoneImageView;
    @BindView(R.id.phoneTextView)
    TextView phoneTextView;

    @BindView(R.id.email_imageView)
    ImageView email_imageView;
    @BindView(R.id.email_textView)
    TextView email_textView;

    @BindView(R.id.websiteImageView)
    ImageView websiteImageView;
    @BindView(R.id.websiteTextView)
    TextView websiteTextView;

    @BindView(R.id.imageView9)
    ImageView skypeIv;
    @BindView(R.id.skypeTextView)
    TextView skypeTextView;

    @BindView(R.id.birthdayImageView)
    ImageView birthdayImageView;
    @BindView(R.id.birthdayTextView)
    TextView birthdayTextView;

    @BindView(R.id.jobImageView)
    ImageView jobImageView;
    @BindView(R.id.projectTextView)
    TextView projectTextView;

    String firstName = "";
    String lastName = "";
    private int position = 0;
    private SendAction sendAction;

    public static DetailContactFragment newInstance(String firstName, String lastName, int position) {
        Bundle bundle = new Bundle();
        bundle.putString("firstName", firstName);
        bundle.putString("lastName", lastName);
        bundle.putInt("position", position);
        DetailContactFragment fragment = new DetailContactFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public interface SendAction {
        void updateListView(int position);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            firstName = getArguments().getString("firstName");
            lastName = getArguments().getString("lastName");
            position = getArguments().getInt("position");
            presenter.parseContactData(firstName, lastName);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detail_contact_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:

                return true;
            case R.id.action_delete:
                presenter.deleteContact(firstName, lastName);
                return true;
            case R.id.action_edit:
                getBaseActivity().addContent(NewContactFragment.newInstance(firstName, lastName));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_detail_contact;
    }

    @Override
    protected int onCreateToolbarTitle() {
        return R.string.gefira_contacts;
    }

    @Override
    protected int onCreateToolbarBackground() {
        return 0;
    }

    @Override
    protected BaseContactPresenter onCreateFeedPresenter() {
        return null;
    }

    @Override
    public void showContactName(String name) {
        if (!TextUtils.isEmpty(name)) {
            fullNameTv.setText(name);
        }
    }

    @Override
    public void showContactPhoneNumbers(String phoneNumbers) {
        if (!TextUtils.isEmpty(phoneNumbers)) {
            phoneTextView.setText(phoneNumbers);
            phoneImageView.setVisibility(View.VISIBLE);
        } else {
            phoneImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showContactEmail(String email) {
        if (!TextUtils.isEmpty(email)) {
            email_textView.setText(email);
            email_imageView.setVisibility(View.VISIBLE);
        } else {
            email_imageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showContactWeb(String web) {
        if (!TextUtils.isEmpty(web)) {
            websiteTextView.setText(web);
            websiteImageView.setVisibility(View.VISIBLE);
        } else {
            websiteImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showContactSkype(String skype) {
        if (!TextUtils.isEmpty(skype)) {
            skypeTextView.setText(skype);
            skypeIv.setVisibility(View.VISIBLE);
        } else {
            skypeIv.setVisibility(View.GONE);
        }
    }

    @Override
    public void showContactBirthday(String birthday) {
        if (!TextUtils.isEmpty(birthday)) {
            birthdayTextView.setText(birthday);
            birthdayImageView.setVisibility(View.VISIBLE);
        } else {
            birthdayImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showContactProjectName(String projectName) {
        if (!TextUtils.isEmpty(projectName)) {
            projectTextView.setText(projectName);
            jobImageView.setVisibility(View.VISIBLE);
        } else {
            jobImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void removeFragment() {
        Toast.makeText(getContext(), R.string.contact_deleted, Toast.LENGTH_LONG).show();
        getBaseActivity().removeCurrentFragment();
        sendAction.updateListView(position);
    }
}
