package com.example.ponomarenkopavel.gefira_redminecontacts.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.example.ponomarenkopavel.gefira_redminecontacts.BuildConfig;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.component.AppComponent;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.component.DaggerAppComponent;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.module.AppModule;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.module.SharedPreferencesModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

public class App extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initTimber();
        initComponent();
        initRealm();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void initComponent() {
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .sharedPreferencesModule(new SharedPreferencesModule())
                .build();
    }

    private void initRealm() {
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    public static AppComponent getApplicationComponent() {
        return appComponent;
    }
}
