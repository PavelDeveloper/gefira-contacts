package com.example.ponomarenkopavel.gefira_redminecontacts.ui.holder;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.app.App;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.MyFragmentManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.ContactItemViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.activity.BaseActivity;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment.DetailContactFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactItemHolder extends BaseViewHolder<ContactItemViewModel> {

    @Inject
    MyFragmentManager myFragmentManager;

    @BindView(R.id.nameTextView)
    TextView tvName;

    @BindView(R.id.phoneTextView)
    TextView tvPhone;

    @BindView(R.id.webSiteTextView)
    TextView website;

    private int position;

    public ContactItemHolder(@NonNull View itemView, int position) {
        super(itemView);
        App.getApplicationComponent().inject(this);
        ButterKnife.bind(this, itemView);
        this.position = position;
    }

    @Override
    public void bindViewHolder(ContactItemViewModel contactItemViewModel) {
        tvName.setText(contactItemViewModel.getFullName());
        tvPhone.setText(contactItemViewModel.getPhone());
        website.setText(contactItemViewModel.getWebsite());

        itemView.setOnClickListener(v -> myFragmentManager.addFragment((BaseActivity) v.getContext(),
                DetailContactFragment.newInstance(contactItemViewModel.getFirstName(),
                        contactItemViewModel.getLastName(), position),
                R.id.main_wrapper));
    }

    @Override
    public void unBindViewHolder() {
        tvName.setText(null);
        tvPhone.setText(null);
        website.setText(null);
    }
}
