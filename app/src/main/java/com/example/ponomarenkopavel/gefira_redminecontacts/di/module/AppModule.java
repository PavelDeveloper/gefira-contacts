package com.example.ponomarenkopavel.gefira_redminecontacts.di.module;

import android.app.Application;
import android.content.Context;
import android.view.LayoutInflater;

import com.example.ponomarenkopavel.gefira_redminecontacts.di.scopes.MyApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @MyApplicationScope
    @Provides
    public Context provideContext() {
        return application;
    }

    @Provides
    @MyApplicationScope
    LayoutInflater provideLayoutInflater() {
        return (LayoutInflater) application.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

}
