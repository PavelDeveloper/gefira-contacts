package com.example.ponomarenkopavel.gefira_redminecontacts.common.managger;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.ponomarenkopavel.gefira_redminecontacts.BuildConfig;
import com.example.ponomarenkopavel.gefira_redminecontacts.app.App;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;
import javax.inject.Inject;
import io.reactivex.Observable;
import io.realm.internal.Util;

public class NetworkManager {

    @Inject
    Context context;

    private static final String TAG = "NetworkManager";

    public NetworkManager() {
        App.getApplicationComponent().inject(this);
    }

    public boolean isOnLine() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return ((networkInfo != null && networkInfo.isConnected()) || Util.isEmulator());
    }

    public Callable<Boolean> isRedmineRechableCallable() {
        return new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                try {
                    if (!isOnLine()) {
                        return false;
                    }
                    URL url = new URL(BuildConfig.BASE_URL);
                    HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                    urlc.setConnectTimeout(2000);
                    urlc.connect();
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        };
    }

    public Observable<Boolean> getNetworkObservable() {
        return Observable.fromCallable(isRedmineRechableCallable());
    }
}
