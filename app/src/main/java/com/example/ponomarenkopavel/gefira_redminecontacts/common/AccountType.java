package com.example.ponomarenkopavel.gefira_redminecontacts.common;

public enum AccountType {

    Client ("Client"),
    Staff ("Staff (internal staff)"),
    Supplier ("Supplier (external specialists)"),
    Company ("New company");

    private final String name;

    AccountType(String str) {
        name = str;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
