package com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.app.App;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.MyFragmentManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.NetworkManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Contact;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.MainView;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import timber.log.Timber;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    @Inject
    MyFragmentManager myFragmentManager;

    @Inject
    NetworkManager mNetworkManager;

    public MainPresenter() {
        App.getApplicationComponent().inject(this);
    }

    public void saveToDb(RealmObject item) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm1.copyToRealmOrUpdate(item);
            Timber.d("Saved to db");
        });
    }

    public Callable<List<Contact>> getListFromRealmCallable() {
        return () -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<Contact> realmResults = realm.where(Contact.class)
                    .sort("date", Sort.DESCENDING)
                    .findAll();
            return realm.copyFromRealm(realmResults);
        };
    }

}
