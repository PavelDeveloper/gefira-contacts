package com.example.ponomarenkopavel.gefira_redminecontacts.di.component;

import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.NetworkManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PhoneBookContactManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.module.AppModule;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.module.ManagerModule;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.module.RestModule;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.module.SharedPreferencesModule;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.scopes.MyApplicationScope;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.ContactListPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.LoginPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.MainPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.NewContactPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.activity.BaseActivity;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.activity.MainActivity;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.holder.ContactItemHolder;

import dagger.Component;


@Component(modules = { AppModule.class, SharedPreferencesModule.class,
        ManagerModule.class, RestModule.class})
@MyApplicationScope
public interface AppComponent {

    //activities
    void inject(BaseActivity activity);
    void inject(MainActivity activity);

    //fragments
    /*void inject(LoginFragment fragment);
    void inject(ContactsListFragment contactsListFragment);
    void inject(NewContactFragment newContactFragment);
    void inject(DetailContactFragment detailContactFragment);
    void inject(ChoiceAccountTypeFragment choiceAccountTypeFragment);*/

    // presenters
    void inject(MainPresenter presenter);
    void inject(LoginPresenter presenter);
    void inject(ContactListPresenter contactListPresenter);
    void inject(NewContactPresenter newContactPresenter);

    // managers
    void inject(NetworkManager manager);
    void inject(PhoneBookContactManager manager);

    // holders
    void inject(ContactItemHolder contactItemHolder);

}
