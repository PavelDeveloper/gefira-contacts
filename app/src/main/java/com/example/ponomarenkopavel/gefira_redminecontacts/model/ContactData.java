
package com.example.ponomarenkopavel.gefira_redminecontacts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactData {

    @SerializedName("contact")
    @Expose
    private Contact contact = null;

    @SerializedName("contacts")
    @Expose
    public List<Contact> contacts = null;

    @SerializedName("issue")
    @Expose
    private Contact contactIssue = null;

    @SerializedName("total_count")
    @Expose
    private Integer totalCount;
    @SerializedName("offset")
    @Expose
    private Integer offset;
    @SerializedName("limit")
    @Expose
    public Integer limit;

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Contact contacts) {
        this.contact = contacts;
    }

    public Contact getContactIssue() {
        return contactIssue;
    }

    public void setContactIssue(Contact contactIssue) {
        this.contactIssue = contactIssue;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
