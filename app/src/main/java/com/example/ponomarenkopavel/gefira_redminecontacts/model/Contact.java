
package com.example.ponomarenkopavel.gefira_redminecontacts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Contact extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private Integer contactId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("author")
    @Expose
    private Author author;
    @SerializedName("address")
    @Expose
    private Address address;
   /* @SerializedName("tag_list")
    @Expose
    private RealmList<Object> tagRealmList = null;*/
    @SerializedName("custom_fields")
    @Expose
    private RealmList<CustomField> customFields = null;
    @SerializedName("created_on")
    @Expose
    private String createdOn;
    @SerializedName("updated_on")
    @Expose
    private String updatedOn;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("skype_name")
    @Expose
    private String skypeName;
    @SerializedName("phone")
    @Expose
    private String phoneStr = null;
    @SerializedName("email")
    @Expose
    private String emailStr = null;
    @SerializedName("phones")
    @Expose
    public RealmList<Phone> phones = null;
    @SerializedName("emails")
    @Expose
    public RealmList<Email> emails = null;
    @SerializedName("is_company")
    @Expose
    private boolean isCompany;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("middle_name")
    @Expose
    private String middleName;
    @SerializedName("job_title")
    @Expose
    private String jobTitle;
    @SerializedName("assigned_to_id")
    @Expose
    private Integer assignedToId;
    @SerializedName("background")
    @Expose
    private String background;
    @SerializedName("avatar")
    @Expose
    private Avatar avatar;
    @SerializedName("assigned_to")
    @Expose
    private AssignedTo assignedTo;
    @SerializedName("project_id")
    @Expose
    private String projectId;
    @SerializedName("projects")
    @Expose
    public RealmList<Project> projects = null;
    @SerializedName("is_private")
    @Expose
    private Boolean isPrivate;
    @SerializedName("tracker_id")
    @Expose
    private Integer trackerId;
    @SerializedName("status_id")
    @Expose
    private Integer statusId;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("priority_id")
    @Expose
    private Integer priorityId;
    @SerializedName("assignee")
    @Expose
    private Integer assignee;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("department")
    @Expose
    private String department;
    @SerializedName("function")
    @Expose
    private String function;
    @SerializedName("first_working_day")
    @Expose
    private String firstWorkingDay;
    @SerializedName("official_entry_date")
    @Expose
    private String officialEntryDate;

    @SerializedName("virtualWorkplace")
    @Expose
    private String virtualWorkplace;

    private String accountType;
    private String _id;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getFirstWorkingDay() {
        return firstWorkingDay;
    }

    public void setFirstWorkingDay(String firstWorkingDay) {
        this.firstWorkingDay = firstWorkingDay;
    }

    public String getOfficialEntryDate() {
        return officialEntryDate;
    }

    public void setOfficialEntryDate(String officialEntryDate) {
        this.officialEntryDate = officialEntryDate;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

   /* public RealmList<Object> getTagRealmList() {
        return tagRealmList;
    }

    public void setTagRealmList(RealmList<Object> tagRealmList) {
        this.tagRealmList = tagRealmList;
    }*/

    public RealmList<CustomField> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(RealmList<CustomField> customFields) {
        this.customFields = customFields;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSkypeName() {
        return skypeName;
    }

    public void setSkypeName(String skypeName) {
        this.skypeName = skypeName;
    }

    public String getPhoneStr() {
        return phoneStr;
    }

    public void setPhoneStr(String phoneStr) {
        this.phoneStr = phoneStr;
    }

    public String getEmailStr() {
        return emailStr;
    }

    public void setEmailStr(String emailStr) {
        this.emailStr = emailStr;
    }

    public RealmList<Phone> getPhones() {
        return phones;
    }

    public void setPhones(RealmList<Phone> phones) {
        this.phones = phones;
    }

    public RealmList<Email> getEmails() {
        return emails;
    }

    public void setEmails(RealmList<Email> emails) {
        this.emails = emails;
    }

    public boolean getIsCompany() {
        return isCompany;
    }

    public void setIsCompany(boolean isCompany) {
        this.isCompany = isCompany;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    public AssignedTo getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(AssignedTo assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Boolean getPrivate() {
        return isPrivate;
    }

    public void setPrivate(Boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public Integer getTrackerId() {
        return trackerId;
    }

    public void setTrackerId(Integer trackerId) {
        this.trackerId = trackerId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(Integer priorityId) {
        this.priorityId = priorityId;
    }

    public Integer getAssignee() {
        return assignee;
    }

    public void setAssignee(Integer assignee) {
        this.assignee = assignee;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAssignedToId() {
        return assignedToId;
    }

    public void setAssignedToId(Integer assignedToId) {
        this.assignedToId = assignedToId;
    }

    public RealmList<Project> getProjects() {
        return projects;
    }

    public void setProjects(RealmList<Project> projects) {
        this.projects = projects;
    }

    public String getVirtualWorkplace() {
        return virtualWorkplace;
    }

    public void setVirtualWorkplace(String virtualWorkplace) {
        this.virtualWorkplace = virtualWorkplace;
    }



    public boolean isClientDataAdded() {
        return !(firstName.equals("") ||
                lastName.equals("") ||
                emailStr.equals("")||
                phoneStr.equals(""));
    }

    public boolean isContactCompanyAdded() {
        return !(firstName.equals("") ||
                emailStr.equals("")||
                phoneStr.equals(""));
    }

    public boolean isIssureDataAdded() {
        return !(subject.equals("") ||
                emailStr.equals("") ||
                phoneStr.equals("") ||
                firstName.equals("") ||
                lastName.equals("") ||
                country.equals("") ||
                department.equals("") ||
                function.equals("") ||
                birthday.equals("") ||
                firstWorkingDay.equals("") ||
                officialEntryDate.equals("") ||
                description.equals(""));
    }

    public void setDescription() {
        this.description =
                "Phone: " + phoneStr + ", " +
                        "Email: " + emailStr + ", " +
                        "First Name: " + firstName + ", " +
                        "Last Name: " + lastName + ", " +
                        "Date of Birth: " + birthday + ", " +
                        "Legal Entity (Country): " + country + ", " +
                        "Department: " + department + ", " +
                        "Function / Job Title: " + function + ", " +
                        "First Working Day: " + firstWorkingDay + ", " +
                        "Official Entry Date: " + officialEntryDate;
    }
}
