package com.example.ponomarenkopavel.gefira_redminecontacts.rest.api;

public class ApiMethods {

    public static final String CONTACTS_GET = "contacts.json";
    public static final String CONTACTS_ADD = "contacts.json";
    public static final String ISSUES_ADD = "issues.json";
    public static final String LOGIN = "contacts.json";
    public static final String CONTACT_EDIT = "contacts/{id}.json";
    public static final String CONTACT_DELETE = "contacts/{id}.json";



}
