package com.example.ponomarenkopavel.gefira_redminecontacts.ui.activity;

import android.os.Bundle;

import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.app.App;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PrefManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.MainView;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment.ContactsListFragment;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment.LoginFragment;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements MainView {

    @Inject
    PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getApplicationComponent().inject(this);

        if (!prefManager.getToken().equals("") && prefManager.getToken() != null) {
            setContent(new ContactsListFragment());
           // setContent(new NewContactFragment());
        } else {
            setContent(new LoginFragment());
        }
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.activity_main;
    }

   /* @Override
    public void showFragment(BaseFragment baseFragment) {
        setContent(baseFragment);
    }*/

}
