package com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter;

import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Contact;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Email;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Phone;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.DetailView;

import io.realm.Realm;
import io.realm.RealmResults;

@InjectViewState
public class DetailContactPresenter extends MvpPresenter<DetailView> {

    private Realm realm;
    private Contact contact;

    public DetailContactPresenter() {
        realm = Realm.getDefaultInstance();
    }

    public void parseContactData(String firstName, String lastName) {

        Contact contact = getContact(firstName, lastName);

        getViewState().showContactName(getName(contact));
        getViewState().showContactPhoneNumbers(getPhoneStr(contact));
        getViewState().showContactEmail(getEmailStr(contact));
        getViewState().showContactWeb(contact.getWebsite());
        getViewState().showContactSkype(contact.getSkypeName());
        getViewState().showContactBirthday(contact.getBirthday());
        getViewState().showContactProjectName(getProjectName(contact));
    }

    public void deleteContact(String firstName, String lastName) {
        RealmResults<Contact> results = realm.where(Contact.class)
                .equalTo("firstName", firstName)
                .and()
                .equalTo("lastName", lastName)
                .findAllAsync();
        realm.executeTransaction(realm -> {
          boolean isDeleted = results.deleteFirstFromRealm();
            if (isDeleted) {
                getViewState().removeFragment();
            }
        });
    }

    private String getName(Contact contact) {
        if (!TextUtils.isEmpty(contact.getLastName())) {
            return contact.getFirstName() + " " + contact.getLastName();
        }
        return contact.getFirstName();
    }

    private String getPhoneStr(Contact contact) {
        StringBuilder phoneStr = new StringBuilder();
        if (contact.getPhones() != null && contact.getPhones().size() > 0) {
            for (Phone number : contact.getPhones()) {
                phoneStr.append(number.getNumber());
                phoneStr.append(" ");
            }
        }
        return phoneStr.toString().trim();
    }

    private String getEmailStr(Contact contact) {
        StringBuilder emailStr = new StringBuilder();
        if (contact.getEmails() != null && contact.getEmails().size() > 0) {
            for (Email mail : contact.getEmails()) {
                emailStr.append(mail.getAddress());
                emailStr.append(" ");
            }
        }
        return emailStr.toString().trim();
    }

    private String getProjectName(Contact contact) {
        if (contact.getProjects().size() > 0 && !TextUtils.isEmpty(contact.getProjects().get(0).getName())) {
            return contact.getProjects().get(0).getName();
        }
        return "";
    }

    private Contact getContact(String firstName, String lastName) {
       contact = realm.where(Contact.class)
                .equalTo("firstName", firstName)
                .and()
                .equalTo("lastName", lastName)
                .findFirst();
        assert contact != null;
        return realm.copyFromRealm(contact);
    }
}
