package com.example.ponomarenkopavel.gefira_redminecontacts.rest.interceptor;

import android.content.Context;
import android.text.TextUtils;

import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PrefManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

public class TokenInterceptor implements Interceptor {

    PrefManager preferenceManager;
    Context context;

    public TokenInterceptor(Context context , PrefManager preferenceManager) {
        this.preferenceManager = preferenceManager;
        this.context = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();
        final String token = preferenceManager.getToken().trim();
        if (!TextUtils.isEmpty(token)) {
            builder.addHeader("Authorization", token);
            Timber.d("TOKEN %s", token);
        }
        return chain.proceed(builder.build());
    }
}
