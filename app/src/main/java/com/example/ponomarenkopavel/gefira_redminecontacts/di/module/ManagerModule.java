package com.example.ponomarenkopavel.gefira_redminecontacts.di.module;

import android.content.Context;

import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.MyFragmentManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.NetworkManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PhoneBookContactManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.scopes.MyApplicationScope;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

@Module
public class ManagerModule {

    @MyApplicationScope
    @Provides
    MyFragmentManager provideMyManager() {
        return new MyFragmentManager();
    }

    @MyApplicationScope
    @Provides
    NetworkManager provideNetworkManager() {
        return new NetworkManager();
    }


    @MyApplicationScope
    @Provides
    @Inject
    PhoneBookContactManager providePhoneBookContactManager(Context context) {
        return new PhoneBookContactManager(context);
    }
}
