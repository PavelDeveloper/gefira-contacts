package com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.consts.AppConstants;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.BaseContactPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.MainView;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.activity.BaseActivity;

public abstract class BaseFragment extends MvpAppCompatFragment implements MainView {

    @LayoutRes
    protected abstract int getMainContentLayout();

    @StringRes
    protected abstract int onCreateToolbarTitle();

    @ColorRes
    protected abstract int onCreateToolbarBackground();

    protected ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getMainContentLayout(), container, false);
        progressBar = getBaseActivity().getProgressBar();
        return view;
    }

    public String createToolbarTitle(Context context) {
        return context.getString(onCreateToolbarTitle());
    }

    public int createToolbarBackground(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && onCreateToolbarBackground() != 0) {
            return context.getColor(onCreateToolbarBackground());
        } else {
            return context.getColor(R.color.primary);
        }
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    protected abstract BaseContactPresenter onCreateFeedPresenter();

    public boolean isContactPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission( getContext(), Manifest.permission.READ_CONTACTS) !=
                    PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission( getContext(),
                    Manifest.permission.WRITE_CONTACTS) !=
                    PackageManager.PERMISSION_GRANTED)) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_CONTACTS) || ActivityCompat
                        .shouldShowRequestPermissionRationale(getActivity(),
                                Manifest.permission.WRITE_CONTACTS)) {

                    AlertDialog.Builder builder = new AlertDialog.Builder( getContext());
                    builder.setTitle("Contacts access needed");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setMessage("please confirm Contacts access");//TODO put real question
                    builder.setOnDismissListener(dialog -> requestPermissions(
                            new String[]
                                    {Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS}
                            , AppConstants.WRITE_CONTACTS_REQUEST));
                    builder.show();
                    return false;
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS},
                            AppConstants.WRITE_CONTACTS_REQUEST);
                    return false;
                }
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    protected void checkPermission() {
        if (ContextCompat.checkSelfPermission( getContext().getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission( getContext().getApplicationContext(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstants.EXTERNAL_STORAGE_REQUEST);
        }
    }

    protected boolean isCameraPermissionGranted() {
        if (ContextCompat.checkSelfPermission( getContext().getApplicationContext(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, AppConstants.CAMEA_REQUEST);
            return false;
        } else {
            return true;
        }
    }

}
