package com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.example.ponomarenkopavel.gefira_redminecontacts.app.App;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PhoneBookContactManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PrefManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.BaseViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.ContactItemViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.BaseContactView;
import com.example.ponomarenkopavel.gefira_redminecontacts.rest.api.ApiService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.realm.RealmObject;

@InjectViewState
public class LoginPresenter extends BaseContactPresenter<BaseContactView> {

    @Inject
    ApiService apiService;

    @Inject
    PrefManager prefManager;

    @Inject
    PhoneBookContactManager phoneBookContactManager;

    public LoginPresenter() {
        App.getApplicationComponent().inject(this);
    }

    @Override
    public Observable<BaseViewModel> onCreateLoadDataObservable(int count, int offset) {
        return apiService.getContacts()
                .flatMap(contactData -> Observable.fromIterable(contactData.getContacts()))
                .doOnNext(this::saveToDb)
                .flatMap(contact -> {
                    List<ContactItemViewModel> list = new ArrayList<>();
                    list.add(new ContactItemViewModel(contact));
                    return Observable.fromIterable(list);
                });
    }

    @Override
    public Observable<BaseViewModel> onCreateRestoreDataObservable() {
        return null;
    }

}
