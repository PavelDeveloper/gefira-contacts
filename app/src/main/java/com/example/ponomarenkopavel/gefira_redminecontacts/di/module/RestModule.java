package com.example.ponomarenkopavel.gefira_redminecontacts.di.module;

import android.content.Context;

import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PrefManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.scopes.MyApplicationScope;
import com.example.ponomarenkopavel.gefira_redminecontacts.rest.RestClient;
import com.example.ponomarenkopavel.gefira_redminecontacts.rest.api.ApiService;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

@Module
public class RestModule {

   private RestClient restClient;

    public RestModule() {
        restClient =  new RestClient();
    }

    @Provides
    @MyApplicationScope
    public RestClient provideRestClient() {
        return restClient;
    }

    @Provides
    @MyApplicationScope
    @Inject
    public ApiService provideApiService(Context context, PrefManager prefManager) {
        return restClient.createService(ApiService.class, context, prefManager);
    }
}
