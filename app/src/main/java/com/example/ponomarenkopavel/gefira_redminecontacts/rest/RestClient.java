package com.example.ponomarenkopavel.gefira_redminecontacts.rest;

import android.content.Context;

import com.example.ponomarenkopavel.gefira_redminecontacts.BuildConfig;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PrefManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.rest.interceptor.TokenInterceptor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {

    public <S> S createService(Class<S> serviceClass, Context context, PrefManager prefManager) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient(context, prefManager))
                .build()
                .create(serviceClass);
    }

    private OkHttpClient getClient(Context context, PrefManager prefManager) {
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new TokenInterceptor(context, prefManager))
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }
}
