package com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.AccountType;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.BaseContactPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ChoiceAccountTypeFragment extends BaseFragment {

    @BindView(R.id.textView2)
    TextView accountTypeTv;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_choice_account_type;
    }

    @Override
    protected int onCreateToolbarTitle() {
        return R.string.choice_account;
    }

    @Override
    protected int onCreateToolbarBackground() {
        return 0;
    }

    @Override
    protected BaseContactPresenter onCreateFeedPresenter() {
        return null;
    }

    @OnClick({R.id.clientButton, R.id.staffButton, R.id.supplierButton, R.id.newCompanyButton})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.clientButton:
                getBaseActivity().addContent(NewContactFragment.newInstance(AccountType.Client));
                break;
            case R.id.staffButton:
                getBaseActivity().addContent(NewContactFragment.newInstance(AccountType.Staff));
                break;
            case R.id.supplierButton:
                getBaseActivity().addContent(NewContactFragment.newInstance(AccountType.Supplier));
                break;
            case R.id.newCompanyButton:
                getBaseActivity().addContent(NewContactFragment.newInstance(AccountType.Company));
                break;
        }
    }
}
