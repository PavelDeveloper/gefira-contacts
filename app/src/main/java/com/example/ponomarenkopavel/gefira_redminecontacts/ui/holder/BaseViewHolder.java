package com.example.ponomarenkopavel.gefira_redminecontacts.ui.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class BaseViewHolder<Item> extends RecyclerView.ViewHolder {

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public abstract void bindViewHolder(Item item);

    public abstract void unBindViewHolder();
}
