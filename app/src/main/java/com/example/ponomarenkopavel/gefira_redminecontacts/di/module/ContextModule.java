package com.example.ponomarenkopavel.gefira_redminecontacts.di.module;

import android.content.Context;

import com.example.ponomarenkopavel.gefira_redminecontacts.di.scopes.MyApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {

    private Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @MyApplicationScope
    Context provideContext() {
        return context;
    }
}
