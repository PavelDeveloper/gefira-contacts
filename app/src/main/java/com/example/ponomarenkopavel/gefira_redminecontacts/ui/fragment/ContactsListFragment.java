package com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment;


import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.adapters.AppBaseAdapter;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.consts.AppConstants;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.MyLinearLayoutManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.BaseViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.BaseContactPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.ContactListPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.BaseContactView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactsListFragment extends BaseFragment implements BaseContactView {

    @InjectPresenter
    ContactListPresenter presenter;

    @BindView(R.id.rv_list)
    RecyclerView recyclerView;

    @BindView(R.id.add_button)
    FloatingActionButton floatingAddButton;

    @BindView(R.id.camera_button)
    FloatingActionButton camera_button;

    @BindView(R.id.phone_book_button)
    FloatingActionButton phone_book_button;

    @BindView(R.id.contact_button)
    FloatingActionButton contact_button;

    private AppBaseAdapter adapter;
    private Animation fabOpenAnimation;
    private Animation fabCloseAnimation;
    private boolean isFabMenuOpen = false;
    private List<BaseViewModel> items;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        presenter = (ContactListPresenter) onCreateFeedPresenter();
        initAnimations();
        setUpRecyclerView();
        setUpAdapter();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isContactPermissionGranted()) {
            presenter.loadDbData();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case AppConstants.WRITE_CONTACTS_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter.loadDbData();
                } else {
                    Toast.makeText(getContext(), R.string.no_permissions, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_contacts_list;
    }

    @Override
    protected int onCreateToolbarTitle() {
        return R.string.contacts;
    }

    @Override
    protected int onCreateToolbarBackground() {
        return 0;
    }

    @Override
    protected BaseContactPresenter onCreateFeedPresenter() {
        return presenter;
    }

    @Override
    public void showListProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideListProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getBaseActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setItems(List<BaseViewModel> items) {
        this.items = items;
        adapter.setItems(items);
    }

    @Override
    public void addItems(List<BaseViewModel> items) {
        this.items = items;
        adapter.addItems(items);
    }

    @OnClick({R.id.add_button, R.id.camera_button, R.id.phone_book_button, R.id.contact_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_button:
                if (isFabMenuOpen)
                    collapseFabMenu();
                else
                    expandFabMenu();
                break;
            case R.id.camera_button:

                collapseFabMenu();
                break;
            case R.id.phone_book_button:

                collapseFabMenu();
                break;
            case R.id.contact_button:
                getBaseActivity().addContent(new ChoiceAccountTypeFragment());
                collapseFabMenu();
                break;
        }
    }

    protected void setUpAdapter() {
        adapter = new AppBaseAdapter();
        recyclerView.setAdapter(adapter);
    }

    public void setUpRecyclerView() {

        MyLinearLayoutManager myLinearLayoutManager = new MyLinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (myLinearLayoutManager.isOnNextPagePosition()) {
                    presenter.loadNext(adapter.getRealItemCount());
                }
            }
        });
        assert ((SimpleItemAnimator) recyclerView.getItemAnimator()) != null;
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);
    }

    private void initAnimations() {
        fabOpenAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fab_open);
        fabCloseAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fab_close);
    }

    private void expandFabMenu() {
        ViewCompat.animate(floatingAddButton).rotation(45.0F).withLayer().setDuration(300).setInterpolator(new OvershootInterpolator(10.0F)).start();
        // floatingAddButton.startAnimation(fabOpenAnimation);
        camera_button.startAnimation(fabOpenAnimation);
        contact_button.startAnimation(fabOpenAnimation);
        phone_book_button.startAnimation(fabOpenAnimation);

        camera_button.setClickable(true);
        contact_button.setClickable(true);
        phone_book_button.setClickable(true);
        isFabMenuOpen = true;
    }

    private void collapseFabMenu() {
        ViewCompat.animate(floatingAddButton).rotation(0.0F).withLayer().setDuration(300).setInterpolator(new OvershootInterpolator(10.0F)).start();
        //floatingAddButton.startAnimation(fabOpenAnimation);
        camera_button.startAnimation(fabCloseAnimation);
        contact_button.startAnimation(fabCloseAnimation);
        phone_book_button.startAnimation(fabCloseAnimation);

        camera_button.setClickable(false);
        contact_button.setClickable(false);
        phone_book_button.setClickable(false);
        isFabMenuOpen = false;
    }

    public void updateListView(int position)
    {
        adapter.notifyItemChanged(position);
    }
}
