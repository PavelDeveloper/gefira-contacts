package com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.AccountType;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.BaseViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.ContactItemViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.BaseContactPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.NewContactPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.NewContactView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class NewContactFragment extends BaseFragment implements NewContactView {

    @InjectPresenter
    NewContactPresenter presenter;

    @BindView(R.id.accountTypeTextView)
    TextView accountTypeTextView;

    @BindView(R.id.firstNameInputLayout)
    TextInputLayout firstNameInputLayout;

    @BindView(R.id.firstNameEditText)
    EditText firstNameEditText;

    @BindView(R.id.lastNameInputLayout)
    TextInputLayout lastNameInputLayout;

    @BindView(R.id.lastNameEditText)
    EditText lastNameEditText;

    @BindView(R.id.phoneInputLayout)
    TextInputLayout phoneInputLayout;

    @BindView(R.id.phoneEditText)
    EditText phoneEditText;

    @BindView(R.id.emailInputLayout)
    TextInputLayout emailInputLayout;

    @BindView(R.id.emailEditText)
    EditText emailEditText;

    @BindView(R.id.skypeInputLayout)
    TextInputLayout skypeInputLayout;

    @BindView(R.id.skypeEditText)
    EditText skypeEditText;

    @BindView(R.id.websiteInputLayout)
    TextInputLayout websiteInputLayout;

    @BindView(R.id.websiteEditText)
    EditText websiteEditText;

    @BindView(R.id.streetInputLayout)
    TextInputLayout streetInputLayout;

    @BindView(R.id.streetEditText)
    EditText streetEditText;

    @BindView(R.id.cityInputLayout)
    TextInputLayout cityInputLayout;

    @BindView(R.id.cityEditText)
    EditText cityEditText;

    @BindView(R.id.regionInputLayout)
    TextInputLayout regionInputLayout;

    @BindView(R.id.regionEditText)
    EditText regionEditText;

    @BindView(R.id.postIndexInputLayout)
    TextInputLayout postIndexInputLayout;

    @BindView(R.id.postIndexEditText)
    EditText postIndexEditText;

    @BindView(R.id.functionInputLayout)
    TextInputLayout functionInputLayout;

    @BindView(R.id.functionEditText)
    EditText functionEditText;

    @BindView(R.id.countrySpinner)
    Spinner countrySpinner;

    @BindView(R.id.companyName)
    Spinner companyNameSpinner;

    @BindView(R.id.departmentSpinner)
    Spinner departmentSpinner;


    @BindView(R.id.birthDayTextView)
    TextView birthDayTextView;

    @BindView(R.id.firstWorkingDayTextView)
    TextView firstWorkingDayTextView;

    @BindView(R.id.entryDateTextView)
    TextView entryDateTextView;

    @BindView(R.id.workPlace)
    TextView workPlace;


    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @BindView(R.id.yesRadioBtn)
    RadioButton yesRadioBtn;

    @BindView(R.id.noRadioBtn)
    RadioButton noRadioBtn;

    @BindView(R.id.submitBtn)
    Button submitBtn;

    private AccountType accountType;

    public static NewContactFragment newInstance(AccountType accountType) {
        Bundle bundle = new Bundle();
        bundle.putString("accountType", accountType.name());
        NewContactFragment fragment = new NewContactFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static NewContactFragment newInstance(String firstName, String lastName) {
        Bundle bundle = new Bundle();
        bundle.putString("firstName", firstName);
        bundle.putString("lastName", lastName);
        NewContactFragment fragment = new NewContactFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            if (getArguments().containsKey("accountType")) {
                try {
                    accountType = Enum.valueOf(AccountType.class, getArguments().getString("accountType"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (getArguments().containsKey("firstName")) {
                try {
                    String firstName = getArguments().getString("firstName");
                    String lastName = getArguments().getString("lastName");
                    presenter.setContact(firstName, lastName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.detail_contact_menu, menu);
        menu.clear();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        progressBar = getBaseActivity().getProgressBar();
        initRedSymbolEditText();
        initFields();
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_new_contact;
    }

    @Override
    protected int onCreateToolbarTitle() {
        return R.string.new_contact;
    }

    @Override
    protected int onCreateToolbarBackground() {
        return 0;
    }

    @Override
    protected BaseContactPresenter onCreateFeedPresenter() {
        return null;
    }

    @Override
    public void setAccountType(AccountType accountType) {
        accountTypeTextView.setText(accountType.toString());
    }

    @Override
    public void setFistName(String fistName) {
        firstNameEditText.setText(fistName);
    }

    @Override
    public void setLastName(String lastName) {
        lastNameEditText.setText(lastName);
    }

    @Override
    public void setPhones(String phones) {
        phoneEditText.setText(phones);
    }

    @Override
    public void setEmails(String emails) {
        emailEditText.setText(emails);
    }

    @Override
    public void setWebsite(String website) {
        websiteEditText.setText(website);
    }

    @Override
    public void setSkype(String skype) {
        skypeEditText.setText(skype);
    }

    @Override
    public void setStreet(String street) {
        streetEditText.setText(street);
    }

    @Override
    public void setCity(String city) {
        cityEditText.setText(city);
    }

    @Override
    public void setRegion(String region) {
        regionEditText.setText(region);
    }

    @Override
    public void setPostIndex(String postIndex) {
        postIndexEditText.setText(postIndex);
    }

    @Override
    public void setCountry(int position) {
        countrySpinner.setSelection(position);
    }

    @Override
    public void setCompany(int position) {
        companyNameSpinner.setSelection(position);
    }

    @Override
    public void setDepartment(int position) {
        departmentSpinner.setSelection(position);
    }

    @Override
    public void setFunction(String job) {
        functionEditText.setText(job);
    }

    @Override
    public void setBirthday(String birthday) {
        birthDayTextView.setText(birthday);
    }

    @Override
    public void setFirstWorkingDay(String date) {
        firstWorkingDayTextView.setText(date);
    }

    @Override
    public void setOfficialEntryDate(String date) {
        entryDateTextView.setText(date);
    }

    @Override
    public void setVWP(boolean contains) {
        if (contains) {
            radioGroup.check(R.id.yesRadioBtn);
        } else {
            radioGroup.check(R.id.noRadioBtn);
        }
    }

    @Override
    public void showListProgress() {

    }

    @Override
    public void hideListProgress() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void setItems(List<BaseViewModel> items) {
        Set<String> companyList = new HashSet<>();
        for (BaseViewModel model : items) {
            if (model instanceof ContactItemViewModel) {
                ContactItemViewModel contactItemViewModel = (ContactItemViewModel) model;
                companyList.add(contactItemViewModel.getCompany());
            }
        }
        initSpinners(new ArrayList<>(companyList));
    }

    @Override
    public void addItems(List<BaseViewModel> items) {
        Timber.d("hello");
    }

    private void initRedSymbolEditText() {
        presenter.setFirstCharRed(firstNameEditText, R.string.first_name);
        presenter.setFirstCharRed(lastNameEditText, R.string.last_name);
        presenter.setFirstCharRed(phoneEditText, R.string.phone);
        presenter.setFirstCharRed(emailEditText, R.string.email);
    }

    private void initSpinners(ArrayList<String> companiesList) {

        ArrayAdapter<String> companyAdapter = new ArrayAdapter<>(
                getContext(), android.R.layout.simple_spinner_item, companiesList);

        companyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        companyNameSpinner.setAdapter(companyAdapter);

        ArrayAdapter departmentAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item);
        departmentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       /* countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
// TODO: 29.10.2018 Add constants
                switch (position) {
                    case 0:
                        departmentAdapter.clear();
                        break;
                    case 1:
                        departmentAdapter.clear();
                        departmentAdapter.addAll(chDepartmentArray);
                        departmentSpinner.setAdapter(departmentAdapter);
                        break;
                    case 2:
                        departmentAdapter.clear();
                        departmentAdapter.addAll(uaDepartmentArray);
                        departmentSpinner.setAdapter(departmentAdapter);
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/
    }

    private void initFields() {
        if (accountType != null) {
            accountTypeTextView.setText(accountType.toString());
        }
    }
}
