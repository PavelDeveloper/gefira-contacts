package com.example.ponomarenkopavel.gefira_redminecontacts.ui.activity;

import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.app.App;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.MyFragmentManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.MainView;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment.BaseFragment;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment.ContactsListFragment;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment.DetailContactFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends MvpAppCompatActivity implements MainView,
        DetailContactFragment.SendAction {

    @Inject
    MyFragmentManager myFragmentManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.progress)
    protected ProgressBar progressBar;

    @LayoutRes
    protected abstract int getMainContentLayout();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        ButterKnife.bind(this);

        App.getApplicationComponent().inject(this);

        setSupportActionBar(toolbar);

        FrameLayout parent = findViewById(R.id.main_wrapper);
        getLayoutInflater().inflate(getMainContentLayout(), parent);
    }

    @Override
    public void onBackPressed() {
        removeCurrentFragment();
    }

    public void fragmentOnScreen(BaseFragment fragment) {
        setToolbarTitle(fragment.createToolbarTitle(this));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            setToolbarBackground(fragment.createToolbarBackground(this));
        }
    }

    public void setToolbarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    public void setToolbarBackground(int color) {
        if (getSupportActionBar() != null && color != 0) {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
        }
    }

    public void setContent(BaseFragment fragment) {
        myFragmentManager.setFragment(this, fragment, R.id.main_wrapper);
    }

    public void addContent(BaseFragment fragment) {
        myFragmentManager.addFragment(this, fragment, R.id.main_wrapper);
    }

    public boolean removeCurrentFragment() {
        return myFragmentManager.removeCurrentFragment(this);
    }

    public boolean removeFragment(BaseFragment fragment) {
        return myFragmentManager.removeFragment(this, fragment);
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    @Override
    public void updateListView(int position) {
        ContactsListFragment fragment = (ContactsListFragment) myFragmentManager.getFragmentStack().lastElement();
        fragment.updateListView(position);
    }
}
