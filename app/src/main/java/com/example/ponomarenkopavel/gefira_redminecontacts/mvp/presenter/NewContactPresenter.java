package com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.widget.EditText;

import com.arellomobile.mvp.InjectViewState;
import com.example.ponomarenkopavel.gefira_redminecontacts.app.App;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.AccountType;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Contact;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Email;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Phone;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.BaseViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.ContactItemViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.NewContactView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

@InjectViewState
public class NewContactPresenter extends BaseContactPresenter<NewContactView> {

    @Inject
    Context context;

    private Contact contact;

    public NewContactPresenter() {
        App.getApplicationComponent().inject(this);
    }

    @Override
    public Observable<BaseViewModel> onCreateRestoreDataObservable() {
        return Observable.fromCallable(getListFromRealmCallable())
                .flatMap(Observable::fromIterable)
                .flatMap(contact -> Observable.fromIterable(parsePojoModel(contact)));
    }

    @Override
    public Observable<BaseViewModel> onCreateLoadDataObservable(int count, int offset) {
        return null;
    }

    public void setFirstCharRed(EditText editText, int hintTextResource) {
        SpannableString ss = new SpannableString(context.getString(hintTextResource));
        ss.setSpan(new ForegroundColorSpan(Color.RED), 0, 1, 0);
        editText.setHint(ss);
    }

    private List<ContactItemViewModel> parsePojoModel(Contact contact) {
        List<ContactItemViewModel> baseItems = new ArrayList<>();
        baseItems.add(new ContactItemViewModel(contact));
        return baseItems;
    }

    public Callable<List<Contact>> getListFromRealmCallable() {
        return () -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<Contact> realmResults = realm.where(Contact.class)
                    .sort("company", Sort.DESCENDING)
                    //.equalTo("isCompany", true)
                    .findAll();
            return realm.copyFromRealm(realmResults);
        };
    }

    public void setContact(String firstName, String lastName) {
        contact = getContact(firstName, lastName);
        if (contact != null) {
            getViewState().setAccountType(getAccountType(contact.getAccountType()));
            getViewState().setFistName(contact.getFirstName());
            getViewState().setLastName(getField(contact.getLastName()));
            getViewState().setPhones(getPhoneStr(contact));
            getViewState().setEmails(getEmailStr(contact));
            getViewState().setWebsite(getField(contact.getWebsite()));
            getViewState().setSkype(getField(contact.getSkypeName()));
            getViewState().setStreet(getField(contact.getAddress().getFullAddress().getStreet1()));
            getViewState().setCity(getField(contact.getAddress().getFullAddress().getCity()));
            getViewState().setRegion(getField(contact.getAddress().getFullAddress().getRegion()));
            getViewState().setPostIndex(getField(contact.getAddress().getFullAddress().getPostcode()));
            // todo: add country, company, department spinners
            getViewState().setFunction(getField(contact.getFunction()));
            getViewState().setBirthday(getField(contact.getBirthday()));
            getViewState().setFirstWorkingDay(getField(contact.getFirstWorkingDay()));
            getViewState().setOfficialEntryDate(getField(contact.getOfficialEntryDate()));
            getViewState().setVWP(isVWPContains(contact.getVirtualWorkplace()));
        }
    }

    private String getField(String field) {
        if (!TextUtils.isEmpty(field)) {
            return field;
        }
        return "";
    }

    private boolean isVWPContains(String vwp) {
        if (!TextUtils.isEmpty(vwp)) {
            switch (vwp) {
                case "0":
                    return false;
                case "1":
                    return true;
                default:
                    return false;
            }
        }
       return false;
    }

    private AccountType getAccountType(String type) {
        if (!TextUtils.isEmpty(type)) {
            return Enum.valueOf(AccountType.class, type);
        }
        return AccountType.Staff;
    }

    private String getPhoneStr(Contact contact) {
        StringBuilder phoneStr = new StringBuilder();
        if (contact.getPhones() != null && contact.getPhones().size() > 0) {
            for (Phone number : contact.getPhones()) {
                phoneStr.append(number.getNumber());
                phoneStr.append(" ");
            }
        }
        return phoneStr.toString().trim();
    }

    private String getEmailStr(Contact contact) {
        StringBuilder emailStr = new StringBuilder();
        if (contact.getEmails() != null && contact.getEmails().size() > 0) {
            for (Email mail : contact.getEmails()) {
                emailStr.append(mail.getAddress());
                emailStr.append(" ");
            }
        }
        return emailStr.toString().trim();
    }

    private String getProjectName(Contact contact) {
        if (contact.getProjects().size() > 0 && !TextUtils.isEmpty(contact.getProjects().get(0).getName())) {
            return contact.getProjects().get(0).getName();
        }
        return "";
    }

    private Contact getContact(String firstName, String lastName) {
        Realm realm = Realm.getDefaultInstance();
        Contact contact = realm.where(Contact.class)
                .equalTo("firstName", firstName)
                .and()
                .equalTo("lastName", lastName)
                .findFirst();
        assert contact != null;
        return realm.copyFromRealm(contact);
    }
}
