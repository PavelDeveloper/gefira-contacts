package com.example.ponomarenkopavel.gefira_redminecontacts.ui.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.BaseViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.BaseContactPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter.LoginPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.BaseContactView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class LoginFragment extends BaseFragment implements BaseContactView {

    @InjectPresenter
    LoginPresenter presenter;

    @BindView(R.id.loginInputLayout)
    TextInputLayout loginInputLayout;

    @BindView(R.id.loginEditText)
    EditText loginEditText;

    @BindView(R.id.passwordInputLayout)
    TextInputLayout passwordInputLayout;

    @BindView(R.id.passwordEditText)
    EditText passwordEditText;

    protected ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        progressBar = getBaseActivity().getProgressBar();
    }

    @Override
    protected int getMainContentLayout() {
        return R.layout.fragment_login;
    }

    @Override
    protected int onCreateToolbarTitle() {
        return R.string.empty_text;
    }

    @Override
    protected int onCreateToolbarBackground() {
        return R.color.color_toolbar_black;
    }

    @Override
    protected BaseContactPresenter onCreateFeedPresenter() {
        return presenter;
    }

    @OnClick(R.id.sign_button)
    public void onClick() {
        if (TextUtils.isEmpty(loginEditText.getText().toString())) {
            loginInputLayout.setError(getString(R.string.invalid_login));
        } else {
            loginInputLayout.setError(getString(R.string.empty_text));
        }
            if (TextUtils.isEmpty(passwordEditText.getText().toString())) {
            passwordInputLayout.setError(getString(R.string.invalid_password));
        } else  {
                passwordInputLayout.setError(getString(R.string.empty_text));
        }
        if (!TextUtils.isEmpty(loginEditText.getText().toString()) &&
                !TextUtils.isEmpty(passwordEditText.getText().toString())) {
            presenter.loadStart(loginEditText.getText().toString(),
                    passwordEditText.getText().toString());
        }
    }

    @Override
    public void showListProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideListProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getBaseActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setItems(List<BaseViewModel> items) {
        getBaseActivity().setContent(new ContactsListFragment());
    }

    @Override
    public void addItems(List<BaseViewModel> items) {
        Timber.d("hello");
    }
}
