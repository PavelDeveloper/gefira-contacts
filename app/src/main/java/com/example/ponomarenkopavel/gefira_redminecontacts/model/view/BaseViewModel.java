package com.example.ponomarenkopavel.gefira_redminecontacts.model.view;

import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.BaseContactView;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.holder.BaseViewHolder;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.holder.ContactItemHolder;

public abstract class BaseViewModel implements BaseContactView {

    public abstract LayoutTypes getType();

    public BaseViewHolder createViewHolder(ViewGroup parent) {
        return onCreateViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contact_body, parent, false));
    }

    protected abstract ContactItemHolder onCreateViewHolder(View view);

    public enum LayoutTypes {
        ContactItemBody(R.layout.item_contact_body);

        private final int id;

        LayoutTypes(int resId) {
            this.id = resId;
        }

        @LayoutRes
        public int getValue() {
            return id;
        }
    }

    public boolean isItemDecorator() {
        return false;
    }
}
