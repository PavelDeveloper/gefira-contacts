package com.example.ponomarenkopavel.gefira_redminecontacts.rest.api;

import com.example.ponomarenkopavel.gefira_redminecontacts.model.ContactData;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;


public interface ApiService {

    @GET(ApiMethods.LOGIN)
    Observable<Response<Void>> login();

    @GET(ApiMethods.CONTACTS_GET)
    Observable<ContactData> getContacts();

    @POST(ApiMethods.CONTACTS_ADD)
    Observable<ContactData> sendContactData(@Body ContactData contactData);

    @POST(ApiMethods.ISSUES_ADD)
    Observable<ContactData> sendIssuesData(@Body ContactData issueData);

    @PUT(ApiMethods.CONTACT_EDIT)
    Observable<Response<Void>> editRedmineContact(@Path("id") int contactId,
                                              @Body ContactData data);
    @DELETE(ApiMethods.CONTACT_DELETE)
    Observable<Response<Void>> deleteContact(@Path("id") int contactId);
}
