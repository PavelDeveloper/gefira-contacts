package com.example.ponomarenkopavel.gefira_redminecontacts.common.managger;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.widget.Toast;

import com.example.ponomarenkopavel.gefira_redminecontacts.R;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Contact;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Email;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Phone;

import java.util.ArrayList;

import timber.log.Timber;

public class PhoneBookContactManager {

   private Context context;

    public PhoneBookContactManager(Context context) {
        this.context = context;
    }

    public void addContactToContacts(Contact model) {
        if (model == null) {
            Timber.d(" Error %s is null!", Contact.class.getName());
            return;
        }

        ArrayList<ContentProviderOperation> operations = new ArrayList<>();
        operations.add(ContentProviderOperation.newInsert(
                ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

//Name
        if (!TextUtils.isEmpty(model.getFirstName())) {
            operations.add(ContentProviderOperation.newInsert(
                    ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(
                            ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                            model.getFirstName() +
                                    " " + (model.getLastName() == null ? "" :
                                    model.getLastName()).trim())
                    .build());
        }

//Number
        if (model.getPhones() != null && !model.getPhones().isEmpty()) {
            for (Phone phone : model.getPhones()) {
                operations.add(ContentProviderOperation.
                        newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone.getNumber())
                        .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                                ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE)
                        .build());
            }
        }

//Email
        if (model.getEmails() != null && !model.getEmails().isEmpty()) {
            for (Email email : model.getEmails()) {
                operations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                        .withValue(ContactsContract.Data.MIMETYPE,
                                ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                        .withValue(ContactsContract.CommonDataKinds.Email.DATA, email.getAddress())
                        .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                        .build());
            }
        }
//URL
        if (!TextUtils.isEmpty(model.getWebsite())) {
            operations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Website.DATA, model.getWebsite())
                    .withValue(ContactsContract.CommonDataKinds.Website.TYPE, ContactsContract.CommonDataKinds.Organization.TYPE_WORK)
                    .withValue(ContactsContract.CommonDataKinds.Website.TYPE, ContactsContract.CommonDataKinds.Website.TYPE_WORK)
                    .build());
        }
        try {
            context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, operations);
        } catch (Exception e) {
            Toast.makeText(context, R.string.error_adding_contact_phone_book, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}
