
package com.example.ponomarenkopavel.gefira_redminecontacts.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class Avatar extends RealmObject  {

    @SerializedName("attachment_id")
    @Expose
    private Integer attachmentId;

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

}
