package com.example.ponomarenkopavel.gefira_redminecontacts.common.consts;

public class AppConstants {

    public static final String PREFERENCES_NAME_KEY = "gefira_contacts_key";
    public static final String TOKEN_KEY = "auth_token";
    public static final String USER_LOGIN_KEY = "user_login";

    public static final String REALM_CONTACT_ID_FIELD_NAME = "contactId";

    public static final int WRITE_CONTACTS_REQUEST = 70;
    public static final int EXTERNAL_STORAGE_REQUEST = 120;
    public static final int CAMEA_REQUEST = 121;
}
