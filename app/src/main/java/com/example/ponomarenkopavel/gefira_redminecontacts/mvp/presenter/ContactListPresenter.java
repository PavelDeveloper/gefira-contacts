package com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.example.ponomarenkopavel.gefira_redminecontacts.app.App;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PhoneBookContactManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PrefManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Contact;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.BaseViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.ContactItemViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.BaseContactView;
import com.example.ponomarenkopavel.gefira_redminecontacts.rest.api.ApiService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

@InjectViewState
public class ContactListPresenter extends BaseContactPresenter<BaseContactView> {

    @Inject
    ApiService apiService;

    @Inject
    PrefManager prefManager;

    @Inject
    PhoneBookContactManager phoneBookContactManager;

    private boolean enableIdFiltering = false;

    public ContactListPresenter() {
        App.getApplicationComponent().inject(this);
    }

    @Override
    public Observable<BaseViewModel> onCreateLoadDataObservable(int count, int offset) {
        return null;
    }

    @Override
    public Observable<BaseViewModel> onCreateRestoreDataObservable() {
        return Observable.fromCallable(getListFromRealmCallable())
                .flatMap(Observable::fromIterable)
               // .compose(applyFilter())
                .flatMap(contact -> Observable.fromIterable(parsePojoModel(contact)));
    }

    private List<BaseViewModel> parsePojoModel(Contact contact) {
        List<BaseViewModel> baseItems = new ArrayList<>();
        baseItems.add(new ContactItemViewModel(contact));
        phoneBookContactManager.addContactToContacts(contact);
        return baseItems;
    }

    public Callable<List<Contact>> getListFromRealmCallable() {
        return () -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<Contact> realmResults = realm.where(Contact.class)
                    .sort("contactId", Sort.DESCENDING)
                    .findAll();
            return realm.copyFromRealm(realmResults);
        };
    }

   /* public void setEnableIdFiltering(boolean enableIdFiltering) {
        this.enableIdFiltering = enableIdFiltering;
    }*/

  /*  protected ObservableTransformer<Contact, Contact> applyFilter() {
        if (enableIdFiltering && Contact.getId() != null) {
            return baseItemObservable -> baseItemObservable.filter(
                    contact -> Contact.getId().equals(String.valueOf(contact.getContactId()))
            );
        } else {
            return baseItemObservable -> baseItemObservable;
        }
    }*/
}
