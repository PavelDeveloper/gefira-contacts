package com.example.ponomarenkopavel.gefira_redminecontacts.mvp.presenter;

import android.util.Base64;

import com.arellomobile.mvp.MvpPresenter;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.NetworkManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.common.managger.PrefManager;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.Contact;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.BaseViewModel;
import com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view.BaseContactView;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;
import timber.log.Timber;

public abstract class BaseContactPresenter<B extends BaseContactView> extends MvpPresenter<B> {

    @Inject
    NetworkManager networkManager;

    @Inject
    PrefManager prefManager;

    private static final int START_PAGE_SIZE = 15;
    private static final int NEXT_PAGE_SIZE = 5;
    private boolean isInLoading;

    public abstract Observable<BaseViewModel> onCreateRestoreDataObservable();

    public abstract Observable<BaseViewModel> onCreateLoadDataObservable(int count, int offset);

    private void loadNetworkData(ProgressType progressType, int offset, int count) {
        if (isInLoading) {
            return;
        }
        isInLoading = true;

        networkManager.getNetworkObservable()
                .flatMap(aBoolean -> {
                    if (!aBoolean && offset > 0) {
                        return Observable.empty();
                    }
                    return aBoolean
                            ? onCreateLoadDataObservable(count, offset)
                            : onCreateRestoreDataObservable();
                })
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    onLoadingStart(progressType);
                })
                .doFinally(() -> {
                    onLoadingFinish(progressType);
                })
                .subscribe(repositories -> {
                    onLoadingSuccess(progressType, repositories);
                }, error -> {
                    error.printStackTrace();
                    onLoadingFailed(error);
                });
    }

    private void loadFromRealm(ProgressType progressType, int offset, int count) {

        onCreateRestoreDataObservable()
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> {
                    onLoadingStart(progressType);
                })
                .doFinally(() -> {
                    onLoadingFinish(progressType);
                })
                .subscribe(repositories -> {
                    onLoadingSuccess(progressType, repositories);
                    if (repositories == null) prefManager.clear();
                }, error -> {
                    error.printStackTrace();
                    onLoadingFailed(error);
                });
    }

    //  private void saveNewContactData(){

    private String getAuthorizationHeader(String login, String password) {
        String credential = login + ":" + password;
        return "Basic " + Base64.encodeToString(credential.getBytes(), Base64.DEFAULT);
    }

    public void loadStart(String login, String password) {
        prefManager.saveToken(getAuthorizationHeader(login,
                password));
        loadNetworkData(ProgressType.ListProgress, 0, START_PAGE_SIZE);
    }

    public void loadDbData() {
        loadFromRealm(ProgressType.ListProgress, 0, START_PAGE_SIZE);
    }

    public void loadNext(int offset) {
        loadNetworkData(ProgressType.Paging, offset, NEXT_PAGE_SIZE);
    }

    public void loadRefresh() {
        loadNetworkData(ProgressType.Refreshing, 0, START_PAGE_SIZE);
    }

    public void onLoadingStart(ProgressType progressType) {
        showProgress(progressType);
    }

    public void onLoadingFinish(ProgressType progressType) {
        isInLoading = false;
        hideProgress(progressType);
    }

    public void onLoadingFailed(Throwable throwable) {
        getViewState().showError(throwable.getLocalizedMessage());
        Timber.d(throwable.getLocalizedMessage());
    }

    public void onLoadingSuccess(ProgressType progressType, List<BaseViewModel> items) {

        if (progressType == ProgressType.Paging) {
            getViewState().addItems(items);
        } else {
            getViewState().setItems(items);
        }
    }

    public void showProgress(ProgressType progressType) {
        switch (progressType) {
           /* case Refreshing:
                getViewState().showRefreshing();
                break;*/
            case ListProgress:
                getViewState().showListProgress();
                break;
        }
    }

    public void hideProgress(ProgressType progressType) {
        switch (progressType) {
           /* case Refreshing:
                getViewState().hideRefreshing();
                break;*/
            case ListProgress:
                getViewState().hideListProgress();
                break;
        }
    }

    public void saveToDb(RealmObject item) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(realm1 -> {
            realm1.copyToRealmOrUpdate(item);
            Timber.d("Saved to db");
        });
    }

    public Callable<List<Contact>> getListFromRealmCallable() {
        return () -> {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<Contact> realmResults = realm.where(Contact.class)
                    .sort("date", Sort.DESCENDING)
                    .findAll();
            return realm.copyFromRealm(realmResults);
        };
    }

    public enum ProgressType {
        Refreshing, ListProgress, Paging,  Companies
    }
}
