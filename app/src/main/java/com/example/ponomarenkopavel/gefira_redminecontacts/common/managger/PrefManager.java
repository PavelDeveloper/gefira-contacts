package com.example.ponomarenkopavel.gefira_redminecontacts.common.managger;

import android.content.SharedPreferences;

import com.example.ponomarenkopavel.gefira_redminecontacts.common.consts.AppConstants;

import javax.inject.Inject;

public class PrefManager {


    private SharedPreferences sharedPreferences;

    @Inject
    public PrefManager(SharedPreferences mSharedPreferences) {
        this.sharedPreferences = mSharedPreferences;
    }

    public void saveToken(String token) {
       sharedPreferences.edit().putString(AppConstants.TOKEN_KEY, token).apply();
    }

    public String getToken() {
        return  sharedPreferences.getString(AppConstants.TOKEN_KEY, "");
    }

    public void saveUserLogin(String login) {
        sharedPreferences.edit().putString(AppConstants.USER_LOGIN_KEY, login).apply();
    }

    public String getLogin() {
        return sharedPreferences.getString(AppConstants.USER_LOGIN_KEY, "");
    }

    public void clear() {
        sharedPreferences.edit().clear().apply();
    }
}
