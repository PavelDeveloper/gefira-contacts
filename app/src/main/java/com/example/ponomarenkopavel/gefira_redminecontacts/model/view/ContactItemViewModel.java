package com.example.ponomarenkopavel.gefira_redminecontacts.model.view;

import android.text.TextUtils;
import android.view.View;

import com.example.ponomarenkopavel.gefira_redminecontacts.model.Contact;
import com.example.ponomarenkopavel.gefira_redminecontacts.ui.holder.ContactItemHolder;

import java.util.List;

public class ContactItemViewModel extends BaseViewModel {

   private String fullName;
   private String firstName;
   private String lastName;
   private String phone;
   private String website;
   private String company;
   private int position;

    public ContactItemViewModel(Contact contact) {
        this.fullName = getFullName(contact);
        this.firstName = contact.getFirstName();
        this.lastName = setLastName(contact);
        this.website = contact.getWebsite();
        this.company = contact.getCompany();

        if (contact.getPhones() != null && contact.getPhones().size() > 0 &&
                contact.getPhones().get(0).getNumber() != null) {
            this.phone = contact.getPhones().get(0).getNumber();
        } else {
            this.phone = "";
        }
    }

    @Override
    public LayoutTypes getType() {
        return LayoutTypes.ContactItemBody;
    }

    @Override
    protected ContactItemHolder onCreateViewHolder(View view) {
        return new ContactItemHolder(view, position);
    }

    private String getFullName(Contact contact) {
        if (!TextUtils.isEmpty(contact.getLastName()) &&
                !contact.getLastName().equals(contact.getFirstName())) {
            return contact.getFirstName() + " " + contact.getLastName();
        }
        return contact.getFirstName();
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    public String getCompany() {
        if (company == null || company.equals("")) {
            return firstName;
        }
        return company;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public void showListProgress() {

    }

    @Override
    public void hideListProgress() {

    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void setItems(List<BaseViewModel> items) {

    }

    @Override
    public void addItems(List<BaseViewModel> items) {

    }

    private String setLastName(Contact contact) {
       return  contact.getLastName() != null ? contact.getLastName() : "";
    }
}
