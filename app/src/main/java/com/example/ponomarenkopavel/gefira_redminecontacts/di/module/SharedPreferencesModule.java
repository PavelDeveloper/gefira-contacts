package com.example.ponomarenkopavel.gefira_redminecontacts.di.module;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.ponomarenkopavel.gefira_redminecontacts.common.consts.AppConstants;
import com.example.ponomarenkopavel.gefira_redminecontacts.di.scopes.MyApplicationScope;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

@Module
public class SharedPreferencesModule {

    @MyApplicationScope
    @Provides
    @Inject
    SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences(AppConstants.PREFERENCES_NAME_KEY, Context.MODE_PRIVATE);
    }
}

