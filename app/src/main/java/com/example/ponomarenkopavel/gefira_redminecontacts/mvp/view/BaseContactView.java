package com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.example.ponomarenkopavel.gefira_redminecontacts.model.view.BaseViewModel;

import java.util.List;

public interface BaseContactView extends MvpView {

    @StateStrategyType(SkipStrategy.class)
    void showListProgress();

    @StateStrategyType(SkipStrategy.class)
    void hideListProgress();

    @StateStrategyType(SkipStrategy.class)
    void showError(String message);

    void setItems(List<BaseViewModel> items);

    void addItems(List<BaseViewModel> items);
}
