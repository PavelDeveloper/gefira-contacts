package com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view;

import com.example.ponomarenkopavel.gefira_redminecontacts.common.AccountType;

public interface NewContactView extends BaseContactView {

    void setAccountType(AccountType accountType);

    void setFistName(String fistName);

    void setLastName(String lastName);

    void setPhones(String phones);

    void setEmails(String emails);

    void setWebsite(String website);

    void setSkype(String skype);

    void setStreet(String street);

    void setCity(String city);

    void setRegion(String region);

    void setPostIndex(String postIndex);

    void setCountry(int position);

    void setCompany(int position);

    void setDepartment(int position);

    void setFunction(String job);

    void setBirthday(String birthday);

    void setFirstWorkingDay(String date);

    void setOfficialEntryDate(String date);

    void setVWP(boolean contains);

}
