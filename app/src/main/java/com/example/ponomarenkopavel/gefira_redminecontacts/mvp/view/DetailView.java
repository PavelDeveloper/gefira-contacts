package com.example.ponomarenkopavel.gefira_redminecontacts.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndStrategy.class)
public interface DetailView extends MvpView {

    void showContactName(String mame);

    void showContactPhoneNumbers(String phoneNumbers);

    void showContactEmail(String email);

    void showContactWeb(String web);

    void showContactSkype(String skype);

    void showContactBirthday(String birthday);

    void showContactProjectName(String projectName);

    void removeFragment();
}
